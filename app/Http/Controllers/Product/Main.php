<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\Products\View as ProductView;
use App\Transformers\Products\Fetch as ProductAll;
use App\Http\Controllers\Product\Helper;
use App\Product;
use App\ProductCategory;
use App\Keranjang;

class Main extends Controller
{
    public function index(Product $produk)
    {
        return fractal()
        ->collection($produk->all())
        ->transformWith(new ProductAll)
        ->toArray();
    }

    public function store(Request $request, Product $produk)
    {
        $produk = $produk->create($request->all());

        return fractal()
        ->item($produk)
        ->transformWith(new ProductView)
        ->addMeta([
            'status' => 'success',
            'produk_id' => $produk->id,
            'kategori_id' => $request['id_produk_kategori']
        ])
        ->toArray();
    }

    public function show(Product $produk)
    {
        return fractal()
        ->item($produk)
        ->transformWith(new ProductView)
        ->addMeta([
            'produk_id' => $produk->id,
            'kategori_id' => $produk->id_produk_kategori
        ])
        ->toArray();
    }

    public function update(Request $request, Product $produk)
    {
        $produk->update($request->all());

        return fractal()
        ->item($produk)
        ->transformWith(new ProductView)
        ->addMeta([
            'produk_id' => $produk->id,
            'kategori_id' => $request['id_produk_kategori']
        ])
        ->toArray();
    }

    public function destroy(Product $produk)
    {
        //
    }

    public function filter(Request $r, Helper $helper)
    {
        dd($helper->zero(2,3));
    }

    public function cart(Request $request, Keranjang $keranjang)
    {
        // $request['id_users'] = $request->user()->id;
        $request['id_users'] = 1;
        $keranjang->create($request->only(['id_users','id_produk','qty']));
        return response()->json('Sukses!');
    }
}
