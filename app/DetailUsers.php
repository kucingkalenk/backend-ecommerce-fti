<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeatilUsers extends Model
{
	protected $table = 'detail_users';
	public $timestamps = false;
	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo('App\User','id_auth');
	}
}
