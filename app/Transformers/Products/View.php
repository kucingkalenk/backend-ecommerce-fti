<?php

namespace App\Transformers\Products;

use League\Fractal\TransformerAbstract;
use App\Product;

class View extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $produk)
    {
        return [
            'nama_barang' => $produk->nama_barang,
            'kategori' => $produk->kategori()->first()->kategori,
            'harga' => $produk->harga,
            'diskon' => $produk->diskon,
            'berat' => $produk->berat,
            'stok' => $produk->stok,
            'deskripsi' => $produk->deskripsi,
            'tag' => $produk->tag,
            'ukuran' => $produk->ukuran
        ];
    }
}
