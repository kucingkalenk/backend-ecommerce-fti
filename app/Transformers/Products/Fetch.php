<?php

namespace App\Transformers\Products;

use League\Fractal\TransformerAbstract;
use App\Product;

class Fetch extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $produk)
    {
        return [
            'id' => $produk->id,
            'nama_barang' => $produk->nama_barang,
            'kategori' => [
                'id' => $produk->id_produk_kategori,
                'name' => $produk->kategori()->first()->kategori,
            ],
            'harga_beli' => $produk->harga_beli,
            'harga_jual' => $produk->harga_jual,
            'diskon' => $produk->diskon,
            'berat' => $produk->berat,
            'stok' => $produk->stok,
            'deskripsi' => $produk->deskripsi,
            'tag' => $produk->tag,
            'ukuran' => $produk->ukuran
        ];
    }
}
