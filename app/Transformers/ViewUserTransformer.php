<?php

namespace App\Transformers;

use App\User;
use App\Akun;
use League\Fractal\TransformerAbstract;


class ViewUserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $akun = Akun::where('id_auth',$user->id)->first();
        return [
            'id' => $user->id,
            'nama' => $akun->nama_lengkap,
            'email' => $user->email
        ];
    }

}