<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	protected $table = 'produk_kategori';
	public $timestamps = false;

	public function produk () {
		return $this->hasMany('App\Product','id_produk_kategori');
	}
}
