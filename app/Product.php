<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'produk';
	protected $guarded = ['id'];
	public $timestamps = false;

	public function kategori () {
		return $this->belongsTo('App\ProductCategory','id_produk_kategori');
	}
}
